require('@lu.se/console');

const
    fastify = require('fastify')({ trustProxy: true }),
    { FastifySSEPlugin } = require('fastify-sse-v2'),
    { EventIterator } = require('event-iterator'),
    googlePlay = require('google-play-scraper').memoized(),
    { default: PQueue } = require('p-queue');

const {
    port,
    matomoUrl,
    matomoSiteId,
    matomoToken,
    sentryDsn
} = require('./config.js');

if(matomoUrl && matomoSiteId && matomoToken){
    fastify.register(require('fastify-matomo'), {
        url: matomoUrl,
        siteId: matomoSiteId,
        token: matomoToken,
        onError: (...args) => console.error(...args),
        hook: 'preHandler'
    });
}
if(sentryDsn)
    fastify.register(require('fastify-sentry'), { dsn: sentryDsn });
fastify.register(require('fastify-cors'));
fastify.register(FastifySSEPlugin);

fastify.get(
    '/search',
    (
        request,
        reply
    ) => {
        const query = request.query.query;
        if(query){
            reply.sse(new EventIterator(({ push: _push }) => {
                const push = data => _push({ data: JSON.stringify(data) });
                (async () => {
                    const
                        search = (await googlePlay.search({
                            'term': request.query.query,
                            'num': 250
                        })).map((item, index) => ({ ...item, index })),
                        queue = new PQueue();
                    await queue.addAll([
                        ...search.map(item => () => push(item)),
                        ...search.map(({ appId }) => async () => push(await googlePlay.app({ appId }))),
                        ...search.map(({ appId }) => async () => push({ appId, permissions: await googlePlay.permissions({ appId }) }))
                    ]);
                })().catch(error => {
                    if([
                        'Error requesting Google Play',
                        'No results for'
                    ].every(message => !error.toString().includes(message)))
                        throw error;
                }).finally(() => push({ status: 'DONE' }));
            }));
        }
        else
            reply.code(400);
    }
);

fastify.get(
    '/link/:linkId',
    (
        request,
        reply
    ) => {
        const url = {
            'about':   'https://git.kaki87.net/playsearch.kaki87.net/v2-web/src/branch/master/README.md',
            'support': 'https://discord.gg/XZ8Ja8d',
            'x3Free':  'https://gist.github.com/KaKi87/d64d7185a8ccb7823d89164c709a6851'
        }[request.params['linkId']];
        if(url)
            reply.redirect(301, url);
        else
            reply.code(404).send();
    }
);

fastify.listen(port).then(() => console.log(`Server listening to port ${port}`)).catch(console.error);