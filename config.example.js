module.exports = {
    /**
     * HTTP server listening port
     * @type {Number}
     */
    port: null,
    /**
     * Matomo URL
     * @type {String}
     */
    matomoUrl: null,
    /**
     * Matomo site ID
     * @type {Number}
     */
    matomoSiteId: null,
    /**
     * Matomo authentication token
     * @type {String}
     */
    matomoToken: null,
    /**
     * Sentry DSN
     * @type {String}
     */
    sentryDsn: null
};